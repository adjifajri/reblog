<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('pages.tampilandepan');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
/* Route::get('/posts/create', 'PostController@create');
Route::post('/posts', 'PostController@store');
Route::get('/posts', 'PostController@index');
Route::get('/posts/{id}', 'PostController@show');
Route::get('/posts/{id}/edit', 'PostController@edit');
Route::put('/posts/{id}', 'PostController@update');
Route::delete('/posts/{id}', 'PostController@destroy'); */

Route::resource('posts','PostController');
Route::resource('profile', 'ProfileController');
Route::post('/comment/store', 'CommentController@store')->name('comment.add');
Route::post('/reply/store', 'ReplyController@replyStore')->name('reply.add');
// Route::post('/komen/{bloge_id}','JawabanController@store');
Route::get('/posts/cetak_pdf', 'PostController@cetak_pdf');
Route::get('users/export/', 'UsersController@export');

Route::group(['prefix' => 'laravel-filemanager', 'middleware' => ['web', 'auth']], function () {
    \UniSharp\LaravelFilemanager\Lfm::routes();
});
