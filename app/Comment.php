<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $table ='comments';
    protected $guarded =[];

    // public function post(){
    //     return $this->belongsTo('App\Post','bloge_id');
    // }

    // public function profile(){
    //     return $this->belongsTo('App\Comment','user_id');
    // }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function replies()
    {
        return $this->BelongsTo('App\Comment', 'bloge_id');
    }
    public function subcomments(){
        return $this->hasMany('App\Subcomment','comment_id');
    }

}
