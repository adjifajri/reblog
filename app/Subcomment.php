<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subcomment extends Model
{
    protected $table ='subcomments';
    protected $guarded =[];

    public function users()
    {
        return $this->belongsTo('App\User');
    }
    public function comments(){
        return $this->belongsTo('App\Comment','comment_id');
    }

}
