<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;
use DB;
use App\Post;
use App\Comment;
use App\Dislike;
use App\Subcomment;
use App\Like;
use Auth;
use App\Profile;
use PDF;


class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth')->except(['index','show']);
    }

    public function index()
    {
        $posts = Post::all();
        return view('pages.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required|max:255',
            'content' => 'required',
        ]);

        $post = Post::create([
            'title' => $request['title'],
            'content' => $request['content'],
            'user_id' => Auth::id()
        ]);

        Alert::success('Berhasil', 'Berhasil Menambahkan Content Baru');
        return redirect('/posts');
    }

    //     $user = Auth::user();
    //     $user->posts()->create([
    //         'title' => $request['title'],
    //         'content' => $request['content']
    //     ]);
    // }

    /**
     * Display the specif ied resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::find($id);
        return view ('pages.show', compact('post'));
    }

        /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::find($id);
        return view ('pages.edit', compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => 'required|max:255',
            'content' => 'required',
        ]);

        // $query = DB::table('pertanyaan')
        //         ->where('id',$id)
        //         ->update([
        //             "judul" => $request["judul"],
        //             "isi" => $request["isi"]
        // ]);

        $update = Post::where('id', $id)->update([
            'title' => $request['title'],
            'content' => $request['content']
        ]);
        return redirect('/posts');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Post::destroy($id);
        return redirect('/posts');
    }

    public function cetak_pdf()
    {
        $post = Post::find($id);

        $pdf = PDF::loadview('pages.index',compact('post'));
        return $pdf->download('invoice.pdf');
    }
}


