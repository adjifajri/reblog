<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = 'bloges';

    protected $guarded = [''];

    public function author ()
    {
        return $this->belongsTo('App\User','user_id');
    }

    public function comments(){
        return $this->hasMany('App\Comment','bloge_id');
    }

    public function sub_comment() {
        return $this->hasMany('App\Comment', 'user_id');
    }
}
