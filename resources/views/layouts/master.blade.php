<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>Reblog Dashboard </title>

  <!-- General CSS Files -->
  <link rel="stylesheet" href="{{url('assets/dist/modules/bootstrap/css/bootstrap.min.css')}}">
  <link rel="stylesheet" href="{{url('assets/dist/modules/fontawesome/css/all.min.css')}}">

  <!-- CSS Libraries -->
  <link rel="stylesheet" href="{{url('assets/dist/modules/jqvmap/assets/dist/jqvmap.min.css')}}">
  <link rel="stylesheet" href="{{url('assets/dist/modules/summernote/summernote-bs4.css')}}">
  <link rel="stylesheet" href="{{url('assets/dist/modules/owlcarousel2/assets/dist/assets/dist/owl.carousel.min.css')}}">
  <link rel="stylesheet" href="{{url('assets/dist/modules/owlcarousel2/assets/dist/assets/dist/owl.theme.default.min.css')}}">

  <!-- Template CSS -->
  <link rel="stylesheet" href="{{url('assets/dist/css/style.css')}}">
  <link rel="stylesheet" href="{{url('assets/dist/css/components.css')}}">
<!-- Start GA -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-94034622-3"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-94034622-3');
</script>
@stack('style')
@stack('script-head')
<!-- /END GA --></head>

<body>
    <div id="app">
        <div class="main-wrapper main-wrapper-1">
          <div class="navbar-bg"></div>
          @include('includes.navbar')
          
          @include('includes.sidebar')
    

      <!-- Main Content -->
      <div class="main-content">
        <section class="section">


          <div class="section-body">
            @yield('content')
          </div>
        </section>
      </div>
      
      @include('includes.footer')
    </div>
  </div>


  <!-- General JS Scripts -->
  <script src="{{url('assets/dist/modules/jquery.min.js')}}"></script>
  <script src="{{url('assets/dist/modules/popper.js')}}"></script>
  <script src="{{('assets/dist/modules/tooltip.js')}}"></script>
  <script src="{{url('assets/dist/modules/bootstrap/js/bootstrap.min.js')}}"></script>
  <script src="{{url('assets/dist/modules/nicescroll/jquery.nicescroll.min.js')}}"></script>
  <script src="{{url('assets/dist/modules/moment.min.js')}}"></script>
  <script src="{{url('assets/dist/js/stisla.js')}}"></script>
  
  <!-- JS Libraies -->
  {{-- <script src="assets/dist/modules/jquery.sparkline.min.js"></script>
  <script src="assets/dist/modules/chart.min.js"></script>
  <script src="assets/dist/modules/owlcarousel2/assets/dist/owl.carousel.min.js"></script>
  <script src="assets/dist/modules/summernote/summernote-bs4.js"></script>
  <script src="assets/dist/modules/chocolat/assets/dist/js/jquery.chocolat.min.js"></script> --}}

  <!-- Page Specific JS File -->
 {{--  <script src="assets/dist/js/page/index.js"></script> --}}
  
  <!-- Template JS File -->
  <script src="{{url('assets/dist/js/scripts.js')}}"></script>
  <script src="{{url('assets/dist/js/custom.js')}}"></script>
  @stack('script')
  @include('sweetalert::alert')
</body>
</html>