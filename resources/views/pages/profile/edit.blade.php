@extends('layouts.master')

@section('content')
<div class="m-3">
  <div class="card card-primary">
    <div class="card-header">
      <h3 class="card-title">Edit Profile {{$items->id}}</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form role="form" action="/profile/{{$items->id}}" method="POST">
    @csrf
    @method('PUT')
      <div class="card-body">
        <div class="form-group">
          <label for="name">Nama Lengkap</label>
          <input type="text" class="form-control" id="name" name="name" value="{{old ('name', $items->name)}}" placeholder="Masukkan nama lengkap">
          @error('nama_lengkap')
            <div class="alert alert-danger">Form nama harus diisi</div>
          @enderror
        </div>
        <div class="form-group">
          <label for="email">Email</label>
          <input type="email" class="form-control" id="email" name="email" value="{{old ('email', $items->email)}}" placeholder="Masukkan email" readonly>
        </div>
      </div>
      <!-- /.card-body -->
      <div class="card-footer">
        <button type="submit" class="btn btn-primary">Update</button>
      </div>
    </form>
  </div>
</div>


@endsection