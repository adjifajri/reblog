@extends('layouts.master')

@section('content')
<br><br><br>
<a class="btn btn-info mb-1" href="{{route('profile.create')}}">Add datadiri</a>
<div class="card mb-3" style="max-width: 540px;">
    <div class="row no-gutters">
      <div class="col-md-4">
        <img src="{{url('assets/dist/img/user2-160x160.jpg')}}" class="card-img">
      </div>
      <div class="col-md-8">
        <div class="card-body">
         @foreach ($items as $item)
         <p class="card-text"> {{$item->name}}</p>
        <p class="card-text"> {{$item->email}}</p>
        <a href="{{route('profile.edit',['profile'=>$item->id])}}" class="btn btn-warning btn-sm">Edit</a>
        <form action="{{route('profile.destroy',['profile'=>$item->id])}}" method="post">
            @csrf
            @method('DELETE')
            <input type="submit" value="delete" class="btn btn-danger btn-sm">
          </form>
         @endforeach
        </div>
        
      </div>
    </div>
    
    
  </div>
@endsection