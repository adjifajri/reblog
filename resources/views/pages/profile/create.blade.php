@extends('layouts.master')

@section('content')
   <div class="ml-3 mt-3">
       <div class="card card-primary">
           <div class="card-header">
               <h3 class="card-title">Buat Profile</h3>
           </div>
           <form action="/profile" method="POST">
            @csrf
            <div class="card-body">
                <div class="form-group">
                    <label for="name">Nama Lengkap</label>
                    <input type="text" class="form-control" id="name" name="name" value="{{ old('name', '') }}" placeholder="Masukkan nama lengkap anda">
                    @error('name')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror      
                </div>
                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" class="form-control" id="email" name="email" value="{{ old('email', '') }}" placeholder="Masukkan email">
                    @error('email')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror      
                </div>
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Buat</button>
            </div>
            </form>
       </div>
   </div>
@endsection