@extends('layouts.master')

@section('content')
<br><br>
    <div class="row artikel">
        <div class="col-md-9 col-sm-12">
            <div class="contentArtikel">
            <h1>{{$post->title}}</h1>
{{--                <figure>--}}
{{--                    <img src="{{asset('artikel/img/1.jpg')}}">--}}
{{--                    <figcaption>Medan 12 january 2020</figcaption>--}}
{{--                </figure>--}}

                <p class="diperbaharui">Diperbaharui : {{$post->updated_at}}</p>

                <div class="teks-artikel">

                   {!!$post->content!!}

{{--                <p>Ultrices natoque mus mattis, aliquam, cras in pellentesque--}}
{{--                    tincidunt elit purus lectus, vel ut aliquet, elementum nunc--}}
{{--                    nunc rhoncus placerat urna! Sit est sed! Ut penatibus turpis--}}
{{--                    mus tincidunt! Dapibus sed aenean, magna sagittis, lorem velit</p>--}}

                </div>

                <div class = "share">
                    <div class="linkshare">
                        <a href="#"><div class="twit"></div></a>
                        <a href="#"><div class="fb"></div></a>
                        <a href="#"><div class="gplus"></div></a>
                    </div>
                </div>

            </div>
        </div>
        <aside class="col-md-3 col-sm-12">
            @include('includes.top')
        </aside>
    </div>
    <h4>Display Comments</h4>
    <div class="col-md-9 col-sm-12">
    <div class="contentArtikel">
                    @include('partials._comment_replies', ['comments' => $post->comments])
                   
                </div>
    </div>
    <h4>Add comment</h4>
    <div class="col-md-9 col-sm-12">
        <div class="contentArtikel">
                    <form method="post" action="{{ route('comment.add') }}">
                        @csrf

                        <div class="form-group">
                            <input type="text" name="comment_body" class="form-control" />
                            <input type="hidden" name="post_id" value="{{ $post->id }}" />
                        </div>
                        {{-- @error('content')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror --}}
                        <div class="form-group">
                            <input type="submit" class="btn btn-warning" value="Add Comment" />
                        </div>
                    </form>
                </div>
            </div>
    {{-- @include('pages.comment') --}}
    {{-- @include('pages.comment')} --}}

@endsection


@push('style')
    <link rel="stylesheet" href="{{asset('assets/artikel/style2.css')}}">
    <link rel="stylesheet" href="{{asset('assets/artikel/comment.css')}}">
@endpush

