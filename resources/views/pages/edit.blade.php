@extends('layouts.master')

@section('content')

<section class="content">


    <div class="card card-primary">
        <div class="card-header">
          <h3 class="card-title">Edit Post {{$post->id}}  </h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
    <form role="form" action="/posts/{{$post->id}}" method="POST">
            @csrf
            @method('PUT')
          <div class="card-body">
            <div class="form-group">
              <label for="title">Title</label>
            <input type="text" class="form-control" id="title" value="{{old('title', $post->title)}}" name="title" placeholder="Title">
              @error('title')
              <div class="alert alert-danger">{{ $message }}</div>
             @enderror
            </div>

            <div class="form-group">
              <label for="content">Content</label>
              <input type="text" class="form-control" id="content" value="{{old('content', $post->content)}}"   name="content" placeholder="content">
              @error('content')
              <div class="alert alert-danger">{{ $message }}</div>
          @enderror
            </div>
          </div>
            <div class="card-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
          </div>
        </form>
    </section>
      </div>

@endsection
