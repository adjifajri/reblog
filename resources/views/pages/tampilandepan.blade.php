@extends('layouts.master')

@section('content')
<section class="content">
    <br>
    <div class="container-fluid">
        <div class="section-title">
            <h2>List</h2>
        </div>
        <div class="container">
            <ul class="playlist-filter controls">
                <li class="control" data-filter="all">All Item</li>
                <li class="control" data-filter=".genres">Bahasa pemograman</li>
                <li class="control" data-filter=".artists">Komputer</li>
                <li class="control" data-filter=".movies">News</li>

            </ul>
        </div>
        <div class="clearfix"></div>
        <div style= "margin-top : -50px;" class="row playlist-area">
            <div class="mix col-lg-3 col-md-4 col-sm-6 genres">
                @if(isset($id))
                    <a style="text-decoration: none" href = "/posts/">
                        <div class="playlist-item">
                            <img src="{{asset('assets/home/img/playlist/laravel.png')}}" alt="">
                            <h5>Laravel</h5>
                        </div>
                    </a>
                @else
                    <a style="text-decoration: none" href = "/posts/">
                        <div class="playlist-item">
                            <img src="{{asset('assets/home/img/playlist/laravel.png')}}" alt="">
                            <h5>Laravel</h5>
                        </div>
                    </a>
                @endif
            </div>

            <div class="mix col-lg-3 col-md-4 col-sm-6 movies">
                @if(isset($id))
                    <a style="text-decoration: none" href = "{{url('data-artikel/1/'.$id)}}">
                        <div class="playlist-item">
                            <img src="{{asset('assets/home/img/playlist/coding.png')}}" alt="">
                            <h5>Tentang TI</h5>
                        </div>
                    </a>
                @else
                    <a style="text-decoration: none" href = "{{url('data-artikel/1')}}">
                        <div class="playlist-item">
                            <img src="{{asset('assets/home/img/playlist/coding.png')}}" alt="">
                            <h5>Tentang TI</h5>
                        </div>
                    </a>
                @endif
            </div>

            <div class="mix col-lg-3 col-md-4 col-sm-6 movies">
                @if(isset($id))
                    <a style="text-decoration: none" href = "{{url('data-artikel/1/'.$id)}}">
                        <div class="playlist-item">
                            <img src="{{asset('assets/home/img/playlist/si.png')}}" alt="">
                            <h5>Tentang SI</h5>
                        </div>
                    </a>
                @else
                    <a style="text-decoration: none" href = "{{url('data-artikel/1')}}">
                        <div class="playlist-item">
                            <img src="{{asset('assets/home/img/playlist/si.png')}}" alt="">
                            <h5>Tentang SI</h5>
                        </div>
                    </a>
                @endif
            </div>

            <div class="mix col-lg-3 col-md-4 col-sm-6 genres">
                @if(isset($id))
                    <a style="text-decoration: none" href = "{{url('data-artikel/1/'.$id)}}">
                        <div class="playlist-item">
                            <img src="{{asset('assets/home/img/playlist/python.png')}}" alt="">
                            <h5>Python</h5>
                        </div>
                    </a>
                @else
                    <a style="text-decoration: none" href = "{{url('data-artikel/1')}}">
                        <div class="playlist-item">
                            <img src="{{asset('assets/home/img/playlist/python.png')}}" alt="">
                            <h5>Python</h5>
                        </div>
                    </a>
                @endif
            </div>

            <div class="mix col-lg-3 col-md-4 col-sm-6 artists">
                @if(isset($id))
                    <a style="text-decoration: none" href = "{{url('data-artikel/1/'.$id)}}">
                        <div class="playlist-item">
                            <img src="{{asset('assets/home/img/playlist/com.jpg')}}" alt="">
                            <h5>Tentang Komputer</h5>
                        </div>
                    </a>
                @else
                    <a style="text-decoration: none" href = "{{url('data-artikel/1')}}">
                        <div class="playlist-item">
                            <img src="{{asset('assets/home/img/playlist/com.jpg')}}" alt="">
                            <h5>Tentang Komputer</h5>
                        </div>
                    </a>
                @endif
            </div>

            <div class="mix col-lg-3 col-md-4 col-sm-6 genres">
                @if(isset($id))
                    <a style="text-decoration: none" href = "{{url('data-artikel/1/'.$id)}}">
                        <div class="playlist-item">
                            <img src="{{asset('assets/home/img/playlist/js.png')}}" alt="">
                            <h5>Javascript</h5>
                        </div>
                    </a>
                @else
                    <a style="text-decoration: none" href = "{{url('data-artikel/1')}}">
                        <div class="playlist-item">
                            <img src="{{asset('assets/home/img/playlist/js.png')}}" alt="">
                            <h5>Javacript</h5>
                        </div>
                    </a>
                @endif
            </div>

            <div class="mix col-lg-3 col-md-4 col-sm-6 genres">
                @if(isset($id))
                    <a style="text-decoration: none" href = "{{url('/data/1/')}}">
                        <div class="playlist-item">
                            <img src="{{asset('assets/home/img/playlist/c.png')}}" alt="">
                            <h5>Bahasa C</h5>
                        </div>
                    </a>
                @else
                    <a style="text-decoration: none" href = "{{url('/data/1/')}}">
                        <div class="playlist-item">
                            <img src="{{asset('assets/home/img/playlist/c.png')}}" alt="">
                            <h5>Bahasa C</h5>
                        </div>
                    </a>
                @endif
            </div>

        </div>
    </div>
</section>
@endsection
@push('style')
    <!-- Google font -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i&display=swap" rel="stylesheet">
    <!-- Stylesheets -->
   {{--  <link rel="stylesheet" href="{{asset('assets/home/css/bootstrap.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/home/css/font-awesome.min.css')}}"/> --}}
    <link rel="stylesheet" href="{{asset('assets/home/css/owl.carousel.min.css')}}"/>
        <link rel="stylesheet" href="{{asset('assets/home/css/slicknav.min.css')}}"/>
    <!-- Main Stylesheets -->
    <link rel="stylesheet" href="{{asset('assets/home/css/style.css')}}"/>
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
@endpush

@push('script')
    <script src="{{asset('assets/home/js/mixitup.min.js')}}"></script>
    <script src="{{asset('assets/home/js/main.js')}}"></script>
@endpush
