@extends('layouts.master')

@section('content')
<section class="content">

    <div class="card card-primary">
        <div class="card-header">
          <h3 class="card-title">@yield('title')</h3>
        </div>
    <div class="mt-3 ml-3">
        <div class="card">
            <div class="card-header">
              <a href="{{url('/posts/cetak_pdf')}} " class="btn btn-primary" target="_blank">Print as PDF</a>
              <a href="{{url('users/export/')}} " class="btn btn-primary" target="_blank">Download Here</a>
            <a href="{{route('posts.create')}}" class="btn btn-primary">Write Article</a>
            
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table class="table table-bordered">
                <thead>
                  <tr>
                    <th style="width: 10px">No</th>
                    <th>Author</th>
                    <th>Judul</th>
                    <th>Isi</th>
                    <th>Di Buat Tanggal</th>
                    <th>Update</th>
                    <th style="width: 40px">Action</th>
                  </tr>
                </thead>
                <tbody>
    @foreach ($posts as $post)
        <tr>
            <th scope="row"> {{ $loop->iteration }}</th>
            {{-- <td>  {{ $post -> author -> name }} </td> --}}
            <td> {{ $post ->author->name }} </td>
            <td>  {{ $post -> title }} </td>
            <td>  {!! $post -> content !!} </td>
            <td>  {{ $post -> created_at  }} </td>
            <td>  {{ $post -> updated_at  }} </td>
        <td style="display: flex">
          <a href="{{route('posts.show',['post'=>$post->id])}}" class="btn btn-info btn-sm">Show</a>
          <a href="{{route('posts.edit',['post'=>$post->id])}}" class="btn btn-warning btn-sm">Edit</a>
          <form action="{{route('posts.destroy',['post'=>$post->id])}}" method="post">
              @csrf
              @method('DELETE')
              <input type="submit" value="delete" class="btn btn-danger btn-sm">
            </form>
        </td>
        </tr>
    @endforeach
                </tbody>
              </table>
            </div>
        </div>

    </div>
    </div>
    </div>


@endsection
