
<div class = "komentar">
    <hr class="line-koment">
<h5 class="sumKomentar">Komentar - <span class="bilkoment">{{$post->comments->count()}}</span></h5>


  <div class="media-list">
      @foreach ($post as $item)
          <div class="media">
            <a href="#" class="mr-3 mt-3 media-left">
                <img src="{{asset('assets/artikel/img/1.jpg')}}" />
            </a>
          </div>
          <div class="media-body">
            <div class="wrap-body">
            <h1 class="media-heading"></h1>

            <div class="media-chat">
            <div class="comment-date"></div>

            <div class="btn-aksi">
                <a href="">
                    <i class=" btn-like far fa-thumbs-up"></i>
                </a>
                <a href="">
                    <i class=" btn-like far fa-thumbs-down"></i>
                </a>
                <a href="" class="btn btn-warning btn-circle text-uppercase" data-toggle="collapse" href="#collapsekomen">
                    <span class="glyphicon glyphicon-comment"></span> comments
                    <i class="fas fa-reply"></i>
                </a>
            </div>
            </div>
            </div>
            <div class="sub-koment collapse" id="collapsekomen">
                <form action="">
                    @csrf
                    <textarea class="form-control" id="content" name="content"></textarea>
                    <input style="margin-bottom: 20px;margin-top: 10px" class="btn btn-success" type="submit" name="subkomen" id="btn-koment" value="Kirim">
                </form>
            </div>
          </div>
      @endforeach
  </div>
    <div class="form-komentar">
        <form action="/komen/{{$post->id}}" method="post">
            @csrf
                    <textarea class="form-control" id="content" name="content"></textarea>
                    <input class="btn btn-success" type="submit" name="content" id="btn-content" value="Kirim">
            </form>
    </div>
</div>
