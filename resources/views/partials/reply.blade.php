<div class="fitur-chat">
    <div class="btn-action">
        <a href="#"> 
            <span class="glyphicon glyphicon-share-alt"></span>
            <i class=" btn-like far fa-thumbs-up"></i>
        </a>
        <a href="#">
            <span class="glyphicon glyphicon-share-alt"></span>
            <i class="btn-like far fa-thumbs-down"></i>
        </a>
        <a class="btn btn-success" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
            <span class="glyphicon glyphicon-comment"></span>
            <i class="fas fa-reply"></i> 
          </a>
    </div>
    <div class="collapse" id="collapseExample">
        <div class="card card-body">
            <form method="post" action="{{ route('reply.add') }}">
                @csrf

                <div class="form-group">
                    <input type="text" name="content" class="form-control" />
                    
                </div>
                {{-- @error('content')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror --}}
                <div class="form-group">
                    <input type="submit" class="btn btn-warning" value="Add subComment" />
                </div>
            </form>
            </div>
        </div>
</div>